#ifndef _motor_driver_
#define _motor_driver_

// Optimized interface motor class for TB6612GNG driver using Arduino
// Although the driver allows 2 motors conected to it
// the class contains just 1 motor for individual control
// So for 2 motors conected to the driver, 2 instances are needed
//
// See datasheet for further details.

# include "Arduino.h"

#define FORWARD 0
#define REVERSE 1

class motor_driver
{
    private:
        int speed;
        int direction;
        int pin_IN1, pin_IN2, pin_PWM;

    public:

        // Constructor and pin definitions
        motor_driver(int in1, int in2, int pwm)
        {
            pin_IN1 = in1;
            pin_IN2 = in2;
            pin_PWM = pwm;
            pinMode(pin_IN1, OUTPUT);
            pinMode(pin_IN2, OUTPUT);
            pinMode(pin_PWM, OUTPUT);
        }

        // Routine for driving the motor. Sign determines direction.
        // Values range from -255 to 255 (8 bit resolution DAC)
        // Internal values (speed and direction) are saved for
        // control purposes (ability to acces them from outside).
        inline void run(int sp)
        {
            if (sp >= 255) speed = 255;
            if (sp <= -255) speed = -255;
            if (sp > -255 && sp < 255) speed = sp;

            //Positive value = Forward
            if(speed >= 0)
            {
                digitalWrite(pin_IN1, HIGH);
                digitalWrite(pin_IN2, LOW);
                direction = FORWARD;
            }
            //Negative value = Reverse
            else
            {
                digitalWrite(pin_IN1, LOW);
                digitalWrite(pin_IN2, HIGH);
                direction = REVERSE;
                speed *= -1;
            }

            analogWrite(pin_PWM, speed);
        }

        // Pasive break system (natural).
        inline void break_pasive(int break_val)
        {
            digitalWrite(pin_IN1, HIGH);
		    digitalWrite(pin_IN2, HIGH);
		    analogWrite(pin_PWM, break_val);
        }

        // Active break system (forced). Heats the driver 
        inline void break_active(int break_val)
        {
            digitalWrite(pin_IN1, LOW);
	        digitalWrite(pin_IN2, LOW);
	        analogWrite(pin_PWM, break_val);
        }

        inline int getSpeed(){return speed;}
        inline int getDir(){return direction;}
        
};

#endif