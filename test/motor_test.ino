
#define PIN_POT_POWER A0
#define PIN_POT_OUT A1

#define PIN_FLAG 12

#include "motor_driver.h"

// MOTOR LEFT
 # define AIN1 9    // pin 1 for direction of motor_left
 # define AIN2 10    // pin 2 for direction of motor_left
 # define PWMA 6    // pin PWM of motor_left
 // MOTOR RIGHT
 # define BIN1 8   // pin 1 for direction of motor_right
 # define BIN2 7   // pin 2 for direction of motor_right
 # define PWMB 5   // pin PWM of motor_right
  #define STBY 4   // pin for enable/disable TB6612FNG driver

motor_driver motor_left(AIN1,AIN2,PWMA);
motor_driver motor_right(BIN1,BIN2,PWMB);

int power = 0;      // Base power for both motors
                    // Turning made w/ PID output
int output = 0;     // Diferential speed for motors

void setup()
{

    DDRB |= B00000100;  // Digital pin 10

    // Serial.begin(9600);
    pinMode(PIN_FLAG,INPUT_PULLUP);

    pinMode(STBY,OUTPUT);
    digitalWrite(STBY,HIGH);

    pinMode(PIN_POT_POWER, INPUT);
    pinMode(PIN_POT_OUT, INPUT);

    motor_left.break_pasive(0);
    motor_right.break_pasive(0);
}

void loop()
{
    int flag = digitalRead(PIN_FLAG);
    if(flag)
    {
        power = analogRead(PIN_POT_POWER);
        power = map(power, 0, 1023, -255, 255);
        output = analogRead(PIN_POT_OUT);
        output = map(output, 0, 1023, -100, 100);

        PORTB |= B00000100; // Force pin 10 HIGH

        motor_left.run(power + output);
        motor_right.run(power - output);

        PORTB &= B11111011; // Force pin 10 LOW

        // Serial.print(power);
        // Serial.print(",");
        // Serial.println(output);
    }
    else
    {
        motor_left.break_active(100);
        motor_right.break_pasive(100);
    }
}
