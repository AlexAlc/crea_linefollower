
#include "motor_driver.h"

// MOTOR LEFT
 # define AIN1 9    // pin 1 for direction of motor_left
 # define AIN2 10    // pin 2 for direction of motor_left
 # define PWMA 6    // pin PWM of motor_left
 // MOTOR RIGHT
 # define BIN1 8   // pin 1 for direction of motor_right
 # define BIN2 7   // pin 2 for direction of motor_right
 # define PWMB 5   // pin PWM of motor_right
 #define STBY 4    // pin for enable/disable TB6612FNG driver

motor_driver motor_left(AIN1,AIN2,PWMA);
motor_driver motor_right(BIN1,BIN2,PWMB);

int power = 100;      // Base power for both motors
                    // Turning made w/ PID output

#include "QTRSensors.h"

#define NUM_SENSORS             8  // number of sensors used
#define NUM_SAMPLES_PER_SENSOR  4  // average 4 analog samples per sensor reading
#define EMITTER_PIN             2  // emitter is controlled by digital pin 2

// sensors 0 through 5 are connected to analog inputs 0 through 5, respectively
QTRSensorsAnalog qtra((unsigned char[]) {0, 1, 2, 3, 4, 5, 6, 7}, 
  NUM_SENSORS, NUM_SAMPLES_PER_SENSOR, EMITTER_PIN);
unsigned int sensorValues[NUM_SENSORS];

double position;

#include "PID_minimal.h"

#define STRAIGT_LINE 3500  // Setpoint error for straight line
#define TURN_LEFT 2500    // Setpoint error for turning left
#define TURN_RIGHT 3500    // Setpoint error for turning right

//Define Variables we'll be connecting to
double setpoint, input, output;

//Specify the links and initial tuning parameters
//double Kp=0.02, Ki=0.01, Kd=0.001;
double Kp=0.016, Ki=0.005, Kd=0.0016;
PID_minimal PID(&input, &output, &setpoint, Kp, Ki, Kd, DIRECT);

void setup()
{
    //Serial.begin(9600);
    
    motor_left.break_pasive(0);
    motor_right.break_pasive(0);

    pinMode(STBY,OUTPUT);
    digitalWrite(STBY,HIGH);   

    pinMode(13, OUTPUT);
    digitalWrite(13, HIGH);    // turn on Arduino's LED to indicate we are in calibration mode
    for (int i = 0; i < 200; i++)  // make the calibration take about 10 seconds
    {
        qtra.calibrate();       // reads all sensors 10 times at 2.5 ms per six sensors (i.e. ~25 ms per call)
    }
    digitalWrite(13, LOW);     // turn off Arduino's LED to indicate we are through with calibration

    PID.SetOutputLimits(-60, 60);
    
    for(int i =0; i < power; i++)
    {
      motor_left.run(i);
      motor_right.run(i);
      delay(4);
    }
}

void loop()
{
    
    position = qtra.readLine(sensorValues);

    PID.SetMode(AUTOMATIC);
    input = position;

    // Setpoint always constant because is a known value
    // For straight line defined as 0 to follow line centre exactly
    // For turns left & right defines as slightly offset for turning
    // closer to centre of curvature
    setpoint = STRAIGT_LINE;
    //setpoint = TURN_LEFT;
    //setpoint = TURN_RIGHT;

    PID.Compute();

    //Serial.print(position);
    //Serial.print(",");
    //Serial.println(output);
    
    motor_left.run(power + output);
    motor_right.run(power - output);
}
